package com.bazingalabs.socioadvocacy.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.bazingalabs.socioadvocacy.R;
import com.bazingalabs.socioadvocacy.bo.LeaderBoardEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 04/07/15.
 */
public class LeaderAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List entityList;

    private class ViewHolder {

        TextView tv_nm_profile,tv_nm_point;

    }

    public LeaderAdapter(Activity activity, ArrayList entityList) {
        this.activity = activity;
        if (entityList == null) {
            entityList = new ArrayList();
        }
        this.entityList = entityList;
    }
    @Override
    public int getCount() {
        return entityList.size();
    }

    @Override
    public Object getItem(int position) {
        return entityList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.lead_item, null,true);
            holder = new ViewHolder();
            holder.tv_nm_profile = (TextView) convertView.findViewById(R.id.tv_nm_profile);
            holder.tv_nm_point = (TextView) convertView.findViewById(R.id.tv_nm_point);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        LeaderBoardEntity entity = (LeaderBoardEntity) entityList.get(position);
        holder.tv_nm_point.setText(entity.getPoint());
        holder.tv_nm_profile.setText("test1");
        Log.d("Point", "" + entity.getPoint());
        //holder.img_queue.setImageResource(R.drawable.sonytv);
        return convertView;
    }




}

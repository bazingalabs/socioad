package com.bazingalabs.socioadvocacy.adapter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ToggleButton;
import android.widget.VideoView;

import com.bazingalabs.socioadvocacy.Base;
import com.bazingalabs.socioadvocacy.R;
import com.bazingalabs.socioadvocacy.bo.LeaderBoardEntity;
import com.bazingalabs.socioadvocacy.bo.PostEntity;
import com.nostra13.universalimageloader.core.ImageLoader;

import org.w3c.dom.Text;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by kakshilshah on 06/07/15.
 */
public class PostAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List entityList;

    private class ViewHolder {

        ProgressDialog pd;
        LinearLayout BGLayout; // for backgoround Like corner Fb image
        ImageView leftTopImg; // left side top post Image
        TextView titleHeaderMain, postid; // for top bold title
        TextView titleHeaderSub;

        //Image Post Layout
        LinearLayout image_content_layout; //for show image with text set visibility
        ImageView imagePost; //for image post included layout
        TextView tvPostText; //for image textview

        // Text Post Layout
        LinearLayout text_content_layout; //Only text layout
        TextView tv_textContent_layout;

        //Player Post Layout

        LinearLayout player_content_layout;
        VideoView post_video_player;
        TextView tvPlayerPostText;

        //Share Layout
        ToggleButton tbFb, tbTwiteer, tbLinkedIn, tbwhats, tbAQ;

        //Comment area

        LinearLayout commentBox_layout, tweetBox_layout;
        ToggleButton tbLike, tbAddFav;
        EditText txt_comment, txt_retweet;
        SharedPreferences.Editor editor;
        SharedPreferences pref;
        Base base;


    }

    public PostAdapter(Activity activity, ArrayList entityList) {
        this.activity = activity;
        if (entityList == null) {
            entityList = new ArrayList();
        }
        this.entityList = entityList;
    }

    @Override
    public int getCount() {
        return entityList.size();
    }

    @Override
    public Object getItem(int position) {
        return entityList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder = null;
        View view = convertView;
        PostEntity entity = (PostEntity) entityList.get(position);
        String message = entity.getmessage();
        String imageURL = entity.getUrl();
        String created_date = entity.getCreatedTime();
        String postid=entity.getpostid();

        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        if (view == null) {
            view = inflater.inflate(R.layout.postitem, parent, false);
            holder = new ViewHolder();
            holder.tvPostText = (TextView) view.findViewById(R.id.tvPostText);
            holder.imagePost=(ImageView)view.findViewById(R.id.imagePost);
            holder.titleHeaderSub=(TextView)view.findViewById(R.id.titleHeaderSub);
            holder.postid=(TextView)view.findViewById(R.id.post_id);

            view.setTag(holder);

        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.tvPostText.setText(message);
        String formatedDate = created_date.substring(0,10);
        holder.titleHeaderSub.setText(formatedDate);
        Log.d("Message In Adapter**", "" + entity.getmessage());
        Log.d("URL in Adapater**", "" + imageURL);
        Log.d("Created Date:", "" + created_date);
        Log.d("Post Id:",""+postid);
        holder.postid.setText(postid);
        ImageLoader imageLoader = ImageLoader.getInstance(); // Get singleton instance
        imageLoader.displayImage(imageURL, holder.imagePost);

        return view;
    }



}

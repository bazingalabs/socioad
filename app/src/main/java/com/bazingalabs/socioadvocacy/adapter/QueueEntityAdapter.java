package com.bazingalabs.socioadvocacy.adapter;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bazingalabs.socioadvocacy.R;
import com.bazingalabs.socioadvocacy.bo.QueueEntity;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by apple on 04/07/15.
 */
public class QueueEntityAdapter extends BaseAdapter {

    private Activity activity;
    private LayoutInflater inflater;
    private List entityList;

    private class ViewHolder {

        ImageView img_queue;

        TextView tv_queuetext;

    }

    public QueueEntityAdapter(Activity activity, ArrayList entityList) {
        this.activity = activity;
        if (entityList == null) {
            entityList = new ArrayList();
        }
        this.entityList = entityList;
    }
    @Override
    public int getCount() {
        return entityList.size();
    }

    @Override
    public Object getItem(int position) {
        return entityList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        final ViewHolder holder;
        if (inflater == null)
            inflater = (LayoutInflater) activity
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            convertView = inflater.inflate(R.layout.list_item_queue, null,true);
            holder = new ViewHolder();
            holder.tv_queuetext = (TextView) convertView.findViewById(R.id.tv_queuetext);
            holder.img_queue = (ImageView) convertView.findViewById(R.id.img_queue);
            convertView.setTag(holder);

        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        QueueEntity entity = (QueueEntity) entityList.get(position);
        holder.tv_queuetext.setText(entity.getmessage());
        Log.d("Queuetext", "" + entity.getmessage());
        //holder.img_queue.setImageResource(R.drawable.sonytv);
        return convertView;
    }




}

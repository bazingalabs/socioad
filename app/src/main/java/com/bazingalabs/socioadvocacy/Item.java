// Decompiled by Jad v1.5.8e. Copyright 2001 Pavel Kouznetsov.
// Jad home page: http://www.geocities.com/kpdus/jad.html
// Decompiler options: braces fieldsfirst space lnc 

package com.bazingalabs.socioadvocacy;


public class Item
{

    private String createdTime;
    private String imageLink;
    private String mediaUrl;
    private String message;
    private String postId;
    private String text;
    private String tweetId;

    public Item()
    {
    }

    public String getcreatedTime()
    {
        return createdTime;
    }

    public String getimageLink()
    {
        return imageLink;
    }

    public String getmediaUrl()
    {
        return mediaUrl;
    }

    public String getmessage()
    {
        return message;
    }

    public String getpostId()
    {
        return postId;
    }

    public String gettext()
    {
        return text;
    }

    public String gettweetId()
    {
        return tweetId;
    }

    public void setcreatedTime(String s)
    {
        createdTime = s;
    }

    public void setimageLink(String s)
    {
        imageLink = s;
    }

    public void setmediaUrl(String s)
    {
        mediaUrl = s;
    }

    public void setmessage(String s)
    {
        message = s;
    }

    public void setpostId(String s)
    {
        postId = s;
    }

    public void settext(String s)
    {
        text = s;
    }

    public void settweetId(String s)
    {
        tweetId = s;
    }
}

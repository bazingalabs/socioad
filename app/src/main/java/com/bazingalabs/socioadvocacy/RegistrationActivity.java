package com.bazingalabs.socioadvocacy;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class RegistrationActivity extends Activity {

    Button btn_reg_reg;
    EditText TV_Full_Text, TV_Domain, TV_EMAIL, TV_Password, TV_Confirm_Password;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    ProgressDialog pd;
    String success;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        TV_Full_Text = (EditText)findViewById(R.id.txt_full_name_reg);
        TV_Domain =(EditText)findViewById(R.id.txt_Domain_key_reg);
        TV_EMAIL = (EditText)findViewById(R.id.txt_email_reg);
        TV_Password = (EditText)findViewById(R.id.txt_pwd_reg);
        TV_Confirm_Password = (EditText)findViewById(R.id.txt_re_pwd_reg);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();
        btn_reg_reg = (Button) findViewById(R.id.btn_register_reg);

        btn_reg_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Log.d("password",""+TV_Password.getText().toString());
                    if(TV_Password.getText().toString().equals( TV_Confirm_Password.getText().toString()))
                    {
                        (new ExeRegistrationProcess()).execute();
                    }
                else{
                        Toast.makeText(RegistrationActivity.this,"Both password should be same",Toast.LENGTH_SHORT).show();
                        TV_Password.setText("");
                        TV_Confirm_Password.setText("");
                    }

            }
        });

    }
    public class ExeRegistrationProcess extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(RegistrationActivity.this);
            pd.setMessage("Please wait Verifying Login Details...");
            pd.setCancelable(true);
            pd.setCanceledOnTouchOutside(true);
            pd.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                DefaultHttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://api.socioadvocacy.com/user/create?access_token=d481c88e3fa43711314fb0e262ca6a088a7a9415");
                HttpEntity httpEntity = null;
                String response = null;
                Log.d("Pwd**:",""+TV_Password.getText().toString());
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("cid", "8e6c1aa7"));
                nameValuePairs.add(new BasicNameValuePair("email", TV_EMAIL.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("password", TV_Password.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("first_name", TV_Full_Text.getText().toString()));
                    nameValuePairs.add(new BasicNameValuePair("last_name", TV_Domain.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("role", "general"));
                nameValuePairs.add(new BasicNameValuePair("timezone", "Asia/Kolkata"));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                Log.d("Register",""+nameValuePairs);

                // Execute HTTP Post Request
                HttpResponse httpResponse = httpclient.execute(httppost);
                httpEntity = httpResponse.getEntity();
                response = EntityUtils.toString(httpEntity);

                Log.d("Response: ", "> " + response);
                final String TAG_RESULT = "result";
                final String TAG_DATA = "data";

                final String TAG_SUCESS = "success";
                final String TAG_ERROR = "debug";
                final String TAG_TIME = "time";

                // contacts JSONArray
                JSONArray result = null;


                if (response != null) {
                    try {
                        JSONObject jsonObj = new JSONObject(response);

                        // Getting JSON Array node
                        // looping through All Contacts

                        JSONObject newJsonObject = jsonObj.getJSONObject(TAG_RESULT);

                        success = newJsonObject.getString(TAG_SUCESS);
                         Log.d("Reg*****", "" + success);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("ServiceHandler", "Couldn't get any data from the url");
                }

                return null;


            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(Void result) {
            pd.dismiss();
            if ( success == "true") {
                // do something
                Log.d("**", "" + success);

                 Intent in = new Intent(RegistrationActivity.this, SocialLoginActivity.class);
                startActivity(in);
                finish();
                // Log.e("result",""+result);



        }}

    }


}

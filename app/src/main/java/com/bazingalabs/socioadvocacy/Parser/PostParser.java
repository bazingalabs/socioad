package com.bazingalabs.socioadvocacy.Parser;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import com.bazingalabs.socioadvocacy.bo.PostEntity;
import com.bazingalabs.socioadvocacy.bo.PostEntityResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
 * Created by kakshilshah on 06/07/15.
 */

public class PostParser extends BaseParser {
    private static final String JSON_KEY_TWEETID = "tweet_id";
    private static final String JSON_KEY_FACEBOOKID = "facebook_id";
    private static final String JSON_KEY_LINKEDINID = "linkedin_id";

    private static final String JSON_KEY_DATA = "data";
    private static final String JSON_KEY_TEXT = "text";
    private static final String JSON_KEY_CREATED_TIME = "created_time";
    private static final String JSON_KEY_SNETWORK = "snetwork";
    private static final String JSON_KEY_TWITTER = "twitter";
    private static final String JSON_KEY_FACEBOOK = "facebook";
    private static final String JSON_KEY_LINKEDIN = "linkedin";

    private static final String JSON_KEY_SUCCESS = "success";
    private static final String JSON_EXTENDED_ENTITY = "extended_entities";
    private static final String JSON_MEDIA = "media";
    private static final String JSON_URL_IMG = "media_url";
    SharedPreferences pref;
    Context context;
    String social_Netowork_source;

    public PostParser() {
    }

    public PostParser(Context context) {
        this.context = context; // assuming you have a local field named context in adapter's class
    }
    public  PostEntityResponse parseListingResponse(String responseJson) {
        PostEntityResponse postResponse = new PostEntityResponse();
        try {

            Log.d("response", "" + responseJson);
            JSONObject jsonObject = new JSONObject(responseJson);
            if (jsonObject != null) {
                JSONObject newJsonObject = jsonObject.getJSONObject("result");
                String success = newJsonObject.getString(JSON_KEY_SUCCESS);
                Log.d("Sucess**", "" + success);
                postResponse.setSuccess(success);
                Log.d("**", "**");

                JSONObject jsonObjectSocial = newJsonObject.getJSONObject(JSON_KEY_DATA).getJSONObject(JSON_KEY_SNETWORK);



                JSONObject facebookObject = new JSONObject();
                JSONObject twitterObject = new JSONObject();
                JSONObject linkedinObject = new JSONObject();

                boolean facebookBool = false, twitterBool = false, linkedinBool = false;


                try
                {
                    facebookObject =  jsonObjectSocial.getJSONObject(JSON_KEY_FACEBOOK);
                    facebookBool = true;
                }
                catch (Exception e)
                {                    e.printStackTrace();

                }

                try
                {
                    twitterObject =  jsonObjectSocial.getJSONObject(JSON_KEY_TWITTER);
                    twitterBool = true;

                }
                catch (Exception e)
                {                    e.printStackTrace();

                }
                try
                {
                    linkedinObject =  jsonObjectSocial.getJSONObject(JSON_KEY_LINKEDIN);
                    linkedinBool = true;

                }
                catch (Exception e)
                {                    e.printStackTrace();

                }

                ArrayList<PostEntity> responseData = new ArrayList<PostEntity>();


                if(facebookBool)
                {
                    //If Facebook Login
                    pref = context.getSharedPreferences("MyPref",0);
                    String facebookID = pref.getString("facebookID","Error!");
                    JSONArray facebookJSONArray = getJSONArray(facebookObject,facebookID);
                    ArrayList<PostEntity> facebookPostEntityArray = parseJSONArrayToArrayList(facebookJSONArray, "fb");
                    responseData.addAll(facebookPostEntityArray);
                }
                if(twitterBool)
                {
                    String twitterID = "3026244859"; //Extract user's Twitter ID from Shared Preferences
                    JSONArray twitterJSONArray = getJSONArray(twitterObject,twitterID);
                    ArrayList<PostEntity> twitterPostEntityArray = parseJSONArrayToArrayList(twitterJSONArray, "tw");
                    responseData.addAll(twitterPostEntityArray);
                }
                if(linkedinBool)
                {
                    String linkedinID = "abc"; //Extract user's linkedin ID from Shared Preferences
                    JSONArray linkedinJSONArray = getJSONArray(linkedinObject,linkedinID);
                    ArrayList<PostEntity> linkedinPostEntityArray = parseJSONArrayToArrayList(linkedinJSONArray, "ln");
                    responseData.addAll(linkedinPostEntityArray);
                }

                postResponse.setData(responseData);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return postResponse;
    }

    public static ArrayList<PostEntity> parseJSONArrayToArrayList(JSONArray jsonArray, String type) {

        ArrayList<PostEntity> dataObjectsArray = new ArrayList<PostEntity>();

        if (jsonArray != null) {
            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    PostEntity entity = parseJSONObjectToEntity(jsonObject,type);
                    dataObjectsArray.add(entity);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return dataObjectsArray;
    }


    public static PostEntity parseJSONObjectToEntity(JSONObject jsonObject,String type) {
        PostEntity data = new PostEntity();

        if (jsonObject != null) {


            if (type.equals("fb")) {
                data.setpostid(getString(jsonObject, JSON_KEY_FACEBOOKID));
            }
            else if(type.equals("tw"))
            {
                data.setpostid(getString(jsonObject, JSON_KEY_TWEETID));
            }
            else if(type.equals("ln"))
            {
                data.setpostid(getString(jsonObject, JSON_KEY_LINKEDINID));
            }

            data.setmessage(getString(jsonObject, JSON_KEY_TEXT));
            data.setCreatedTime(getString(jsonObject, JSON_KEY_CREATED_TIME));
            data.setsourceType(type);
            try {

                JSONObject mediaJsonObj = jsonObject.getJSONObject(JSON_EXTENDED_ENTITY);
                JSONArray mediaArray = mediaJsonObj.getJSONArray(JSON_MEDIA);
                if (mediaArray.length() > 0) {
                    JSONObject mediaObject = mediaArray.getJSONObject(0);
                    String mediaURL = mediaObject.getString(JSON_URL_IMG);
                    Log.d("Tag", "Media URL - " + mediaURL);
                    data.setUrl(mediaURL);
                }
            } catch (Exception e) {
                e.printStackTrace();

            }
        }
        return data;
    }
}

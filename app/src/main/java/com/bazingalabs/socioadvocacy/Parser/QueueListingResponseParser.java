package com.bazingalabs.socioadvocacy.Parser;

import android.util.Log;

import com.bazingalabs.socioadvocacy.bo.QueueEntity;
import com.bazingalabs.socioadvocacy.bo.QueueListingResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by apple on 04/07/15.
 */
public class QueueListingResponseParser extends BaseParser {

    private static final String JSON_KEY_SID = "sid";
    private static final String JSON_KEY_DATA = "data";
    private static final String JSON_KEY_MESSAGE = "message";
    private static final String JSON_KEY_SCHEDULEDATE = "scheduled_date";
    private static final String JSON_KEY_QUEUESTATUS = "queue_status";
    private static final String JSON_KEY_SUCCESS = "success";
    private static  String msg="";
    public QueueListingResponseParser() {
    }

    //Invoke this method to parse response json string
    public static QueueListingResponse parseListingResponse(String responseJson) {
        QueueListingResponse listingResponse = new QueueListingResponse();
        try {

            Log.d("response", "" + responseJson);
            JSONObject jsonObject = new JSONObject(responseJson);
            if (jsonObject != null) {
                JSONObject newJsonObject = jsonObject.getJSONObject("result");
                String success = newJsonObject.getString("success");
                Log.d("Sucess**", "" + success);
                listingResponse.setSuccess(success);
                Log.d("**", "**");
                listingResponse.setData(parseData(getJSONArray(jsonObject, "data")));
                Log.d("**", "**"+parseData(getJSONArray(jsonObject, "data")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return listingResponse;
    }

    public static ArrayList<QueueEntity> parseData(JSONArray jsonArray) {
        ArrayList<QueueEntity> datas = new ArrayList<QueueEntity>();
        Log.d("@@###", "*&^%$");
        JSONObject jsonObjectNull = new JSONObject();
        if (jsonArray != null)
        {

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    Log.d("**",""+jsonArray.getJSONObject(i).toString());
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    datas.add(parseData(jsonObject));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        else{
            datas.add(parseData(jsonObjectNull));
        }

        return datas;

        }



    public static QueueEntity parseData(JSONObject jsonObject) {
        QueueEntity data = new QueueEntity();
        Log.d("***", "" + data);


        data.setmessage("Sorry, No posts in the Queue");


        Log.d("***", "" + data);
        return data;
    }
}
package com.bazingalabs.socioadvocacy.Parser;

import android.util.Log;

import com.bazingalabs.socioadvocacy.bo.LeaderBoardEntity;
import com.bazingalabs.socioadvocacy.bo.LeaderResponse;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by apple on 04/07/15.
 */
public class LeaderResponseParser extends BaseParser {

    private static final String JSON_KEY_UID = "uid";
    private static final String JSON_KEY_DATA = "data";
    private static final String JSON_KEY_POINTS = "points";
    private static final String JSON_KEY_RANK = "rank";
    private static final String JSON_KEY_SUCCESS = "success";
    private static String msg = "";

    public LeaderResponseParser() {
    }

    //Invoke this method to parse response json string
    public static LeaderResponse parseListingResponse(String responseJson) {
        LeaderResponse leaderResponse = new LeaderResponse();
        try {

            Log.d("response", "" + responseJson);
            JSONObject jsonObject = new JSONObject(responseJson);
            if (jsonObject != null) {
                JSONObject newJsonObject = jsonObject.getJSONObject("result");
                String success = newJsonObject.getString("success");
                Log.d("Sucess**", "" + success);
                leaderResponse.setSuccess(success);
                Log.d("**", "**");
                leaderResponse.setData(parseData(getJSONArray(newJsonObject, "data")));
                Log.d("**", "**" + parseData(getJSONArray(newJsonObject, "data")));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return leaderResponse;
    }

    public static ArrayList<LeaderBoardEntity> parseData(JSONArray jsonArray) {
        ArrayList<LeaderBoardEntity> datas = new ArrayList<LeaderBoardEntity>();
        Log.d("@@###", "*&^%$");
        if (jsonArray != null) {

            for (int i = 0; i < jsonArray.length(); i++) {
                try {
                    Log.d("**", "" + jsonArray.getJSONObject(i).toString());
                    JSONObject jsonObject = jsonArray.getJSONObject(i);
                    datas.add(parseData(jsonObject));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        return datas;

    }


    public static LeaderBoardEntity parseData(JSONObject jsonObject) {
        LeaderBoardEntity data = new LeaderBoardEntity();
        Log.d("***", "" + data);


        //data.setmessage("no post skeduled");

        if (jsonObject != null) {
            data.setrank(getString(jsonObject,JSON_KEY_RANK));
            data.setPoints(getString(jsonObject,JSON_KEY_POINTS));
            data.setuid(getString(jsonObject, JSON_KEY_UID));
        }
        Log.d("***", "" + data);
        return data;
    }
}

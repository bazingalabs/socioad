package com.bazingalabs.socioadvocacy;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by apple on 01/07/15.
 */
public class CustomList extends ArrayAdapter<String> {
    private final Activity context;
    private final String[] data;
    private final Integer[] imageId;

    public CustomList(Activity context,
                      String[] web, Integer[] imageId) {
        super(context, R.layout.list_item_queue, web);
        this.context = context;
        this.data = web;
        this.imageId = imageId;

    }

    @Override
        public View getView(int position, View view, ViewGroup parent) {
            LayoutInflater inflater = context.getLayoutInflater();
            View rowView= inflater.inflate(R.layout.list_item_queue, null, true);
            TextView txtTitle = (TextView) rowView.findViewById(R.id.tv_queuetext);
            ImageView imageView = (ImageView) rowView.findViewById(R.id.img_queue);
            txtTitle.setText(data[position]);
            imageView.setImageResource(imageId[position]);
            return rowView;
        }

}

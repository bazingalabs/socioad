package com.bazingalabs.socioadvocacy;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class LoginActivity extends Activity {

    String access_token = "d481c88e3fa43711314fb0e262ca6a088a7a9415";
    String cid;
    Base base;
    SharedPreferences.Editor editor;
    String fname;
    JSONArray jsonArray;
    JSONObject jsonObject;
    String lname;
    EditText passwordEditText;
    ProgressDialog pd;
    SharedPreferences pref;
    JsonParser jsonParserClass;
    String uid;
    EditText userNameEditText;
    Button btn_login, btn_reg;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        // getActionBar().hide();
        base = new Base(this);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();
        userNameEditText = (EditText) findViewById(R.id.txt_email);
        passwordEditText = (EditText) findViewById(R.id.txt_pwd);
        btn_login = (Button) findViewById(R.id.btn_login);
        btn_reg = (Button) findViewById(R.id.btn_register);

        btn_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(LoginActivity.this, RegistrationActivity.class);
                startActivity(in);
            }
        });

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //Intent in = new Intent(LoginActivity.this,SocialLoginActivity.class);
                //startActivity(in);

                if (userNameEditText.getText().toString().equals("") && passwordEditText.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "Please Enter User Name and Password", Toast.LENGTH_SHORT).show();
                    return;
                } else {
                    (new GetCidDetailsForLogin()).execute();
                    return;
                }
            }
        });
    }

    public class GetCidDetailsForLogin extends AsyncTask<Void, Void, Void> {


        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(LoginActivity.this);
            pd.setMessage("Please wait Verifying Login Details...");
            pd.setCancelable(true);
            pd.setCanceledOnTouchOutside(true);
            pd.show();

        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                DefaultHttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://api.socioadvocacy.com/user/login/?access_token=d481c88e3fa43711314fb0e262ca6a088a7a9415");
                HttpEntity httpEntity = null;
                String response = null;
                // Add your data
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("email", userNameEditText.getText().toString()));
                nameValuePairs.add(new BasicNameValuePair("password", passwordEditText.getText().toString()));

                //  nameValuePairs.add(new BasicNameValuePair("email", "admin1@testing.com"));
                // nameValuePairs.add(new BasicNameValuePair("password", "test123"));

                nameValuePairs.add(new BasicNameValuePair("password_mode", "plain"));
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                // Execute HTTP Post Request
                HttpResponse httpResponse = httpclient.execute(httppost);
                httpEntity = httpResponse.getEntity();
                response = EntityUtils.toString(httpEntity);

                /*BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent(), "iso-8859-1"), 8);
                StringBuilder sb = new StringBuilder();
                sb.append(reader.readLine() + "\n");
                String line = "0";
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                    Log.d("***d:",line);
                }

                reader.close();
                String result11 = sb.toString();

                // parsing data
                return null;*/
                Log.d("Response: ", "> " + response);
// JSON Node names
                final String TAG_RESULT = "result";
                final String TAG_DATA = "data";

                final String TAG_SUCESS = "success";
                final String TAG_ERROR = "debug";
                final String TAG_TIME = "time";

                // contacts JSONArray
                JSONArray result = null;


                if (response != null) {
                    try {
                        JSONObject jsonObj = new JSONObject(response);

                        // Getting JSON Array node
                        // looping through All Contacts

                        JSONObject newJsonObject = jsonObj.getJSONObject(TAG_RESULT).getJSONObject(TAG_DATA);

                        // String success = newJsonObject.getString(TAG_SUCESS);
                        // Log.d("*****", "" + success);
                        //  String data=newJsonObject.getString(TAG_DATA);

                        //Log.d("****",""+data);

                        //   JSONArray jsonarr = newJsonObject.getJSONArray(TAG_DATA);
                        //   for (int n = 0; n < jsonarr.length(); n++) {
                        //  JSONObject j = jsonArray.getJSONObject(0);
                        uid = newJsonObject.getString("uid");
                        cid = newJsonObject.getString("cid");
                        Log.d("uid", "" + uid);
                        Log.d("cid", "" + cid);
                        // }
                        // JSONObject newjob = newJsonObject.getJSONObject(TAG_ERROR);
                        // String time = newjob.getString(TAG_TIME);


                        //  Log.d("*****T:",""+time);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.e("ServiceHandler", "Couldn't get any data from the url");
                }

                return null;


            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(Void result) {
            pd.dismiss();
            if (result == null) {
                // do something
                Log.d("**", "" + result);
                // Log.e("result",""+result);
                Log.e("uid", (new StringBuilder()).append(uid).toString());
                Log.e("cid", (new StringBuilder()).append(cid).toString());
                Log.e("access token", (new StringBuilder()).append(access_token).toString());

                editor.putString("cid", cid);
                editor.putString("access_token", access_token);
                editor.putString("uid", uid);
                editor.putString("login", "success");
                editor.commit();

                String prefcid= pref.getString("cid","");
                Log.d("**Prrefcid",""+prefcid);

                passwordEditText.setText("");
                userNameEditText.setText("");
                if (uid != null && cid != null) {
                    Intent in = new Intent(LoginActivity.this, SocialLoginActivity.class);
                    startActivity(in);
                    finish();
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Login Unsuccessfull", Toast.LENGTH_SHORT).show();
                }

            } else {
                // error occured

                Toast.makeText(LoginActivity.this, "Login Unsuccessfull", Toast.LENGTH_SHORT).show();
            }
        }

    }
}

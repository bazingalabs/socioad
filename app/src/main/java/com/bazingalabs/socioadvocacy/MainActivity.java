package com.bazingalabs.socioadvocacy;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;


public class MainActivity extends Activity {

    private static int SPLASH_TIME_OUT = 5000;

    SharedPreferences.Editor editor;

    String prefcid;
    SharedPreferences pref;
    String socialLoginPref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //  getActionBar().hide();

        // Create global configuration and initialize ImageLoader with this config


        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        prefcid = pref.getString("uid", "");
        editor = pref.edit();

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                // goTOActivity();

                    Intent in = new Intent(MainActivity.this, LoginActivity.class);
                    startActivity(in);
                    finish();

            }
        }, 1000);
    }


}

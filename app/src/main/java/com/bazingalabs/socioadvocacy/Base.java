package com.bazingalabs.socioadvocacy;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by apple on 02/07/15.
 */
public class Base {
    private Activity activity;
    private Context context;
    public Bitmap resizedbitmap;

    public Base(Context context1)
    {
        resizedbitmap = null;
        context = context1;
        activity = (Activity)context;
    }
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
    public boolean checkInternet(Context context)
    {
        boolean status = false;
        ConnectivityManager connectivityManager = (ConnectivityManager) context
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] networkInfos = connectivityManager.getAllNetworkInfo();
        for (NetworkInfo tempNetworkInfo : networkInfos) {

            if (tempNetworkInfo.isConnected()) {
                status = true;
                break;
            }
        }
        return status;
    }


    public void showNoChangesMsg()
    {
        ((Activity)context).runOnUiThread(new Runnable() {


            public void run()
            {
                String s = "NochangeMsg";
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
                Log.e("Error", "No internet connection");
            }

        });
    }

    public void showNoInternetConnection()
    {
        ((Activity)context).runOnUiThread(new Runnable() {


            public void run()
            {
                String s = "noInternet";//context.getString(0x7f060052);
                Toast.makeText(context, s, Toast.LENGTH_SHORT).show();
                Log.e("Error", "No internet connection");
            }
        });
    }
}

/**
 *
 */
package com.bazingalabs.socioadvocacy.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.NameValuePair;
import org.json.JSONArray;

import java.util.List;

/**
 * Class used to perform network operation.
 *
 * @author Praveen
 */
public class NetworkTask extends AsyncTask<Void, Void, String> {
  ProgressDialog pd;
    private Context context;
    private List<NameValuePair> nameValuePairs;
    private RequestCompleteListener listener;
    private String action;
    private String progressMessage;
    private int HTTP_REQUEST_TYPE;
    private String http_get_url = "";
    private JSONArray jsonArray;
    private boolean isProgressVisible = true;



    public NetworkTask(Context context, List<NameValuePair> nameValuePairs, RequestCompleteListener listener,
                       String action, String message, int http_requestType, String url) {
        this.context = context;
        this.nameValuePairs = nameValuePairs;
        this.listener = listener;
        this.action = action;
        this.progressMessage = message;
        this.HTTP_REQUEST_TYPE = http_requestType;
        this.http_get_url = url;
        this.isProgressVisible = true;
    }
    public NetworkTask(Context context, RequestCompleteListener listener,
                       String action, String message, int http_requestType, String url) {
        this.context = context;
        this.listener = listener;
        this.action = action;
        this.progressMessage = message;
        this.HTTP_REQUEST_TYPE = http_requestType;
        this.http_get_url = url;
        this.isProgressVisible = true;
    }


    @Override
    protected void onPreExecute() {
        super.onPreExecute();
       pd = new ProgressDialog(context);
        pd.setMessage("Please wait...");
        pd.setCancelable(false);
        pd.setCanceledOnTouchOutside(false);
        pd.show();
    }


    @Override
    protected String doInBackground(Void... params) {

        String response = "";
        switch (HTTP_REQUEST_TYPE) {
            case Constant.HTTP_NON_MULTIPART: {
                    Log.d("NetworkNameValue***:",""+nameValuePairs);
                    response = HttpClient.sendHttpPostRequest(http_get_url,
                            nameValuePairs);

            }
            break;

            case Constant.HTTP_GET:
                response = HttpClient.sendHttpGetRequest(http_get_url);
                break;
            case Constant.HTTP_POST_JSON:
                response = HttpClient.sendHttpPostJsonRequest(http_get_url, jsonArray);
                break;
            default:
                break;
        }

        return response;
    }

    @Override

    protected void onPostExecute(String result) {
        Log.d("Result on Post ","Exceute of action:"+result);
        super.onPostExecute(result);
        if (pd.isShowing()) {
            pd.dismiss();
        }
        listener.onRequestComplete(result, action);
    }

    public interface RequestCompleteListener {
        public void onRequestComplete(String response, String action);
    }
}

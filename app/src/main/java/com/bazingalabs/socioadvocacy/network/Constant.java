package com.bazingalabs.socioadvocacy.network;

public class Constant {


    public static final int HTTP_MULTIPART = 101;
    public static final int HTTP_NON_MULTIPART = 100;
    public static final int HTTP_GET = 102;
    public static final int HTTP_POST_JSON = 103;

    public static final String URL = "URL";

    public static final String TITLE = "TITLE";


    public static final String SELECTED_POS = "SELECTED_POS";
    public static final int CATEGORY_DRAWER = 1001;
    public static final int MENU_DRAWER = 1002;
   public static final String USERID = "a7a4d02a-9c60-4967-a720-d9d65cc00fef";
  //public static final String USERID = "";
    public static final String ENTITY = "ENTITY";


    public final static int NEAR_BY_LIST = 1;
    public final static int FAVORITE_LIST = 2;
    public final static int RECOMMEND_LIST = 3;
    public final static int TRENDING_LIST = 4;


    public static final String STR_FORMAT_SERVICE_PEOPLE_USE = "%s people used.";
    public static final String STR_FORMAT_SERVICE_PRICE = "Rs. %s";
    public static final String SLOTS_HOURS_DATA = "SLOTS_HOURS_DATA";
    public static final String SLOTS_DATE = "SLOTS_DATE";
    public static final String ENTITY_ID = "ENTITY_ID";
    public static final String SERVICE_ID = "SERVICE_ID";
    public static final String DATE_OF_BOOKING = "dateOfBooking";
    public static final String BOOKING_SECTION = "BOOKING_SECTION";
    public static final String FILTER = "FILTER";


    public static interface API {
        public static final String ADD_TO_FAVORITE = "http://skedulebackend.appspot.com/api/addToFavourite";
        public static final String BASEURL = "http://skedulebackend.appspot.com/api/";
        public static final String BOOK_SLOT = "http://skedulebackend.appspot.com/api/bookSlot";
        public static final String FB_CHECK = "http://skedulebackend.appspot.com/api/facebookCheck";
        public static final String GET_ALL_BOOKING = "http://skedulebackend.appspot.com/api/getAllBooking";
        public static final String GET_LISTING = "http://skedulebackend.appspot.com/api/getListing";
        public static final String GET_OFFER = "http://skedulebackend.appspot.com/api/getOffer";
        public static final String GET_OTHER = "http://skedulebackend.appspot.com/api/";
        public static final String GET_SLOT_DATA = "http://skedulebackend.appspot.com/api/getSlotData";
        public static final String LOAD_DASHBOARD = "http://skedulebackend.appspot.com/api/loadDashboard";
        public static final String LOAD_MORE_LISTING = "http://skedulebackend.appspot.com/api/loadMoreListing";
        public static final String LOAD_MORE_OFFER = "http://skedulebackend.appspot.com/api/";
        public static final String LOGIN = "http://skedulebackend.appspot.com/api/login";
        public static final String RESET_PASSWORD = "http://skedulebackend.appspot.com/api/forgetPassword";
        public static final String SEARCH_QUERY_FULL = "http://skedulebackend.appspot.com/api/searchQuery";
        public static final String SEARCH_QUERY_PARTIAL = "http://skedulebackend.appspot.com/api/searchQuery";
        public static final String SET_RATING = "http://skedulebackend.appspot.com/api/setRatings";
        public static final String SIGN_UP = "http://skedulebackend.appspot.com/api/signup";
        public static final String VERIFY_USER = "http://skedulebackend.appspot.com/api/verifyUser";

    }
    public Constant()
    {
    }
    public static interface CATEGORY
    {

        public static final String ASTROTURFS = "Astroturfs";
        public static final String CAR_CLEANING = "Car Cleaning";
        public static final String CAR_SERVICING = "Car Servicing";
        public static final String GAMING_ZONE = "Gaming Zone";
        public static final String RESTAURANT_TABLES = "Restaurant Tables";
        public static final String SALON = "Salon";
        public static final String SPA = "Spa";
    }
    public static interface API_ACTION {


        public static final String GET_LISTING = "API_ACTION_GETLISTING";

    }
}

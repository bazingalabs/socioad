package com.bazingalabs.socioadvocacy.network;

import android.util.Log;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Praveen
 */
public class HttpClient {

    private static final String TAG = HttpClient.class.getSimpleName();
    public static DefaultHttpClient httpclient;
    public static String cookie = "";

    /**
     * @param currentStateJson as JSONObject
     * @param viewIdJson       as JSONObject
     * @return return String
     */
    public static String sendHttpPostRequest(String urlString, List<NameValuePair> nameValuePairs) {

        HttpPost postRequest = new HttpPost(urlString);
        String responsedata = null;

        try {
            httpclient = new DefaultHttpClient();
            postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
        } catch (Exception e1) {
            e1.printStackTrace();
        }

        postRequest.setHeader("Content-type", "application/x-www-form-urlencoded");
        postRequest.setHeader("Accept", "application/json");

        HttpResponse response = null;

        try {
            response = httpclient.execute(postRequest);
            int responseCode = response.getStatusLine().getStatusCode();
            String msg = response.getStatusLine().getReasonPhrase();

            System.out.println("responseCode" + "************************" + responseCode);
            System.out.println("ResponseMsg" + "*************************" + msg);

            if (responseCode == 200) {

                responsedata = EntityUtils.toString(response.getEntity());
            }

        } catch (Exception e) {
            e.printStackTrace();

        }

        return responsedata;
    }


    public static String sendHttpPostJsonRequest(String urlString, JSONArray jsArray) {

        HttpPost postRequest = new HttpPost(urlString);
        String responsedata = null;

        StringBuilder sb = new StringBuilder();
        sb.append("object=" + jsArray.toString());

        String param = sb.toString();
        Log.d("request param", "->" + param);

        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        nameValuePairs.add(new BasicNameValuePair("object", jsArray.toString()));


        try {
            postRequest.setHeader("Content-type", "application/x-www-form-urlencoded");
            postRequest.setHeader("Accept", "application/json");
            postRequest.setHeader("accept-charset", "UTF-8");
            //postRequest.setHeader("Content-Length", bytecontent.length+"");
            httpclient = new DefaultHttpClient();

            postRequest.setEntity(new UrlEncodedFormEntity(nameValuePairs));
            //postRequest.setEntity(new StringEntity(param));

        } catch (Exception e1) {
            e1.printStackTrace();
        }


        HttpResponse response = null;

        try {
            response = httpclient.execute(postRequest);
            int responseCode = response.getStatusLine().getStatusCode();
            String msg = response.getStatusLine().getReasonPhrase();

            System.out.println("responseCode" + "************************" + responseCode);
            System.out.println("ResponseMsg" + "*************************" + msg);

            if (responseCode == 200) {

                responsedata = EntityUtils.toString(response.getEntity(), HTTP.UTF_8);
            } else {
                // throw server Exception

            }

        } catch (Exception e) {
            e.printStackTrace();

        }

        return responsedata;
    }


    /**
     * @param url as String
     * @return Json String sends HttpGet request
     */
    public static String sendHttpGetRequest(String url)

    {

        String responsedata = null;
        try {
            httpclient = new DefaultHttpClient();
            HttpGet httpGet = new HttpGet(url);
            httpGet.setHeader("Accept", "application/json");
            if (cookie.length() > 1)
                httpGet.setHeader("Cookie", cookie);
            HttpResponse response = httpclient.execute(httpGet);
            if (cookie.length() < 1) {
                Header[] headers = response.getHeaders("Set-Cookie");
                if (headers.length == 1)
                    HttpClient.cookie = headers[0].getValue();
            }

            int responseCode = response.getStatusLine().getStatusCode();
            String msg = response.getStatusLine().getReasonPhrase();

            Log.d("URL", "**************************" + url);
            Log.d("responseCode", "**************************" + responseCode);
            Log.d("ResponseMsg", "**************************" + msg);

            if (responseCode == 200) {
                responsedata = EntityUtils.toString(response.getEntity());
            }

            Log.d("Response Data", "**************************" + responsedata);

        } catch (Exception e) {
            Log.e(TAG, e.getMessage());

        }
        return responsedata;
    }


    public static String convertStreamToString(InputStream is)
            throws IOException {
        StringBuffer writer = new StringBuffer();
        if (is != null) {
            try {
                int iChar = -1;
                do {
                    iChar = is.read();
                    if (iChar != -1)
                        writer.append((char) iChar);
                } while (iChar != -1);
            } catch (Exception e) {
            }
        }
        return writer.toString();
    }


}

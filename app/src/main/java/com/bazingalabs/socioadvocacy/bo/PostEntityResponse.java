package com.bazingalabs.socioadvocacy.bo;

import java.util.ArrayList;

/**
 * Created by kakshilshah on 06/07/15.
 */
public class PostEntityResponse {
    private ArrayList data;
    private String success;
    public PostEntityResponse()
    {

    }
    public ArrayList getData()
    {
        return data;
    }

    public String getSuccess()
    {
        return success;
    }

    public void setData(ArrayList arraylist)
    {
        data = arraylist;
    }

    public void setSuccess(String s)
    {
        success = s;
    }



}

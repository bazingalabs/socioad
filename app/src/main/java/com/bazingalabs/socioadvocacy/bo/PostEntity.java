package com.bazingalabs.socioadvocacy.bo;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by kakshilshah on 06/07/15.
 */
public class PostEntity implements Parcelable {

    String message,postid,created_time,imgurl;
    String sourceType;
    //ArrayList media;
    public String getmessage() {
        return message;
    }

    public void setmessage(String s) {
        message = s;
    }

    public String getsourceType() {
        return sourceType;
    }

    public void setsourceType(String s) {
        sourceType = s;
    }

    public String getpostid() {
        return postid;
    }
    public String getUrl(){return imgurl;}

    public void setUrl(String url){ imgurl = url;}
    public void setpostid(String s) {
        postid = s;
    }

    public String getCreatedTime(){ return created_time;}

    public void setCreatedTime(String ct){ created_time = ct;}

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
    }
}
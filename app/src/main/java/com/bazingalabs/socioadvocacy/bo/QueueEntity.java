package com.bazingalabs.socioadvocacy.bo;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by apple on 04/07/15.
 */
public class QueueEntity implements Parcelable {

    String message;

    public String getmessage()
    {
        return message;
    }

    public void setmessage(String s)
    {
        message = s;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {

    }
}

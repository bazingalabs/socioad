package com.bazingalabs.socioadvocacy.bo;

import java.util.ArrayList;

/**
 * Created by apple on 04/07/15.
 */
public class LeaderResponse {
    private ArrayList data;
    private String success;

    public LeaderResponse() {

    }

    public ArrayList getData() {
        return data;
    }

    public String getSuccess() {
        return success;
    }

    public void setData(ArrayList arraylist) {
        data = arraylist;
    }

    public void setSuccess(String s) {
        success = s;
    }
}

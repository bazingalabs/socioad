package com.bazingalabs.socioadvocacy;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;
import android.widget.VideoView;

import com.bazingalabs.socioadvocacy.Parser.PostParser;
import com.bazingalabs.socioadvocacy.Parser.QueueListingResponseParser;
import com.bazingalabs.socioadvocacy.adapter.PostAdapter;
import com.bazingalabs.socioadvocacy.adapter.QueueEntityAdapter;
import com.bazingalabs.socioadvocacy.bo.PostEntity;
import com.bazingalabs.socioadvocacy.bo.PostEntityResponse;
import com.bazingalabs.socioadvocacy.bo.QueueListingResponse;
import com.bazingalabs.socioadvocacy.network.NetworkTask;
import com.lorentzos.flingswipe.SwipeFlingAdapterView;

import org.apache.http.HttpEntity;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.BasicResponseHandler;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.lang.reflect.Array;
import java.util.ArrayList;


public class PostActivity extends Activity implements NetworkTask.RequestCompleteListener {

    String cid, uid, fb_access_token;
    String access_token = "d481c88e3fa43711314fb0e262ca6a088a7a9415";
    ArrayList<PostEntity> dataObjectsArray;
    SwipeFlingAdapterView flingContainer;

    PostAdapter postadapter;

    SharedPreferences.Editor editor;
    SharedPreferences pref;
    Base base;

    String strDestinationNetwork, strEngagementType;
    ToggleButton tbFb, tbTwiteer, tbLinkedIn, tbAQ, tbWhatsapp;

    @SuppressLint("NewApi")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post);

        try {
            pref = getApplicationContext().getSharedPreferences("MyPref", 0);
            editor = pref.edit();
            View shareView = View.inflate(this, R.layout.sharecontent_layout, null);
            base = new Base(PostActivity.this);
            cid = pref.getString("cid", null);
            uid = pref.getString("uid", null);

            fb_access_token = pref.getString("facebookAccessToken", null);
            flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);
            tbFb = (ToggleButton) findViewById(R.id.tbFb);
            tbAQ = (ToggleButton) findViewById(R.id.tbAQ);
            tbLinkedIn = (ToggleButton) findViewById(R.id.tbLinkedIn);
            tbTwiteer = (ToggleButton) findViewById(R.id.tbTwiteer);
            tbWhatsapp = (ToggleButton) findViewById(R.id.tbwhats);

//            tbFb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//                @Override
//                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                    // Save the state here
//                    Log.e("I Am Tired!","tired--> " + isChecked);
//                }
//            });
//            tbFb.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    Log.e("I Am Tired!","tired--> " );
//
//                }
//            });
           /* CompoundButton.OnCheckedChangeListener listener =
                    new CompoundButton.OnCheckedChangeListener() {
                        @Override
                        public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                            Log.e("I Am Tired!","tired--> " + isChecked);
                        }
                    };

            tbFb.setOnCheckedChangeListener(listener);
*/

            dataObjectsArray = new ArrayList<PostEntity>();

            postadapter = new PostAdapter(PostActivity.this, dataObjectsArray);
            flingContainer = (SwipeFlingAdapterView) findViewById(R.id.frame);
            flingContainer.setAdapter(postadapter);
            flingContainer.setFlingListener(new SwipeFlingAdapterView.onFlingListener()
                                            {
                                                @Override
                                                public void removeFirstObjectInAdapter() {
                                                    dataObjectsArray.remove(0);
                                                    postadapter.notifyDataSetChanged();
                                                }

                                                @Override
                                                public void onLeftCardExit(Object dataObject) {
                                                    Toast.makeText(getApplicationContext(), "Not Shared", Toast.LENGTH_SHORT).show();
                                                }

                                                @Override
                                                public void onRightCardExit(Object dataObject) {
                                                //Imp Changes needed
                                                    PostEntity postObject = (PostEntity) dataObject;
                                                    sharePost(postObject);
                                                }

                                                @Override
                                                public void onAdapterAboutToEmpty(int itemsInAdapter) {
                                                    try {
                                                    } catch (Exception e) {
                                                        Log.e("Error!", "In Empty");
                                                    }
                                                }

                                                @Override
                                                public void onScroll(float scrollProgressPercent) {
                                                    View view = flingContainer.getSelectedView();
                                                    view.findViewById(R.id.item_swipe_right_indicator).setAlpha(scrollProgressPercent < 0 ? -scrollProgressPercent : 0);
                                                    view.findViewById(R.id.item_swipe_left_indicator).setAlpha(scrollProgressPercent > 0 ? scrollProgressPercent : 0);
                                                }
                                            }


            );


            flingContainer.setOnItemClickListener(new SwipeFlingAdapterView.OnItemClickListener() {
                @Override
                public void onItemClicked(int itemPosition, Object dataObject) {

                }
            });

            getPosts();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getPosts() {
        (new NetworkTask(PostActivity.this, this, "API_ACTION_GETLISTING", "Please wait", 102, "http://api.socioadvocacy.com/company/feed/2015-01-01/2015-03-10?cid=" + cid + "&uid=" + uid + "&access_token=" + access_token)).execute(new Void[0]);
    }

    private void sharePost(PostEntity postEntity) {
        Log.e("Facebook1","Sharing on Facebook");
        Log.e("State","States CHECKED FB - " + tbFb.isChecked());
        Log.e("State","States CHECKED TW - " + tbTwiteer.isChecked());
        Log.e("State","States CHECKED LN - " + tbLinkedIn.isChecked());
        Log.e("State","States CHECKED WA - " + tbWhatsapp.isChecked());
        Log.e("State","States CHECKED AQ - " + tbAQ.isChecked());



        if (tbAQ.isChecked()) {
            //Show Queue Pop up
            Log.e("Facebook2","Sharing on Facebook");

            Toast.makeText(getApplicationContext(), "BETA!", Toast.LENGTH_SHORT).show();
        }
        else {
            if (tbFb.isChecked()) {
                //Sharing on Facebook!
                Log.e("Facebook","Sharing on Facebook");
                String checkString = pref.getString("isFacebook","no");
                if (checkString.equals("yes")) {
                    ArrayList arraylist = new ArrayList();
                    arraylist.add(new BasicNameValuePair("cid", cid));
                    arraylist.add(new BasicNameValuePair("uid", uid));
                    arraylist.add(new BasicNameValuePair("source_id", postEntity.getpostid()));
                    arraylist.add(new BasicNameValuePair("source_snetwork", postEntity.getsourceType()));
                    arraylist.add(new BasicNameValuePair("destination_snetwork", "fb"));
                    arraylist.add(new BasicNameValuePair("engagement_type", "share"));
                    arraylist.add(new BasicNameValuePair("text", postEntity.getmessage()));
                    (new NetworkTask(PostActivity.this, arraylist, this, "API_ACTION_SHARE", "Please wait", 100, "http://api.socioadvocacy.com/feed/action?access_token=d481c88e3fa43711314fb0e262ca6a088a7a9415")).execute(new Void[0]);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Please login Via Facebook to share on Facebook!", Toast.LENGTH_SHORT).show();
                }
            }
            if (tbTwiteer.isChecked()) {
                String checkString = pref.getString("isTwitter","no");
                if (checkString.equals("yes")) {
                ArrayList arraylist = new ArrayList();
                arraylist.add(new BasicNameValuePair("cid", cid));
                arraylist.add(new BasicNameValuePair("uid", uid));
                arraylist.add(new BasicNameValuePair("source_id", postEntity.getpostid()));
                arraylist.add(new BasicNameValuePair("source_snetwork", postEntity.getsourceType()));
                arraylist.add(new BasicNameValuePair("destination_snetwork", "tw"));
                arraylist.add(new BasicNameValuePair("engagement_type", "retweet"));
                arraylist.add(new BasicNameValuePair("text", postEntity.getmessage()));
                (new NetworkTask(PostActivity.this, arraylist, this, "API_ACTION_SHARE", "Please wait", 100, "http://api.socioadvocacy.com/feed/action?access_token=d481c88e3fa43711314fb0e262ca6a088a7a9415")).execute(new Void[0]);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Please login Via twitter to share on twitter!", Toast.LENGTH_SHORT).show();
                }
                }
            if (tbLinkedIn.isChecked()) {
                String checkString = pref.getString("isLinkedin","no");
                if (checkString.equals("yes")) {

                ArrayList arraylist = new ArrayList();
                arraylist.add(new BasicNameValuePair("cid", cid));
                arraylist.add(new BasicNameValuePair("uid", uid));
                arraylist.add(new BasicNameValuePair("source_id", postEntity.getpostid()));
                arraylist.add(new BasicNameValuePair("source_snetwork", postEntity.getsourceType()));
                arraylist.add(new BasicNameValuePair("destination_snetwork", "ln"));
                arraylist.add(new BasicNameValuePair("engagement_type", "share"));
                arraylist.add(new BasicNameValuePair("text", postEntity.getmessage()));
                (new NetworkTask(PostActivity.this, arraylist, this, "API_ACTION_SHARE", "Please wait", 100, "http://api.socioadvocacy.com/feed/action?access_token=d481c88e3fa43711314fb0e262ca6a088a7a9415")).execute(new Void[0]);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), "Please login via linkedIn to share on linkedIn!", Toast.LENGTH_SHORT).show();
                }
            }
            if (tbWhatsapp.isChecked()) {
                //Share on Whatsapp
                Intent sendIntent = new Intent();
                sendIntent.setPackage("com.whatsapp");
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, postEntity.getmessage());
                sendIntent.setType("text/plain");
                startActivity(sendIntent);
            }
        }
    }




    @Override
    public void onRequestComplete(String response, String action) {
        if (action.equalsIgnoreCase("API_ACTION_GETLISTING") && response != null) {

            PostParser parser = new PostParser(this);
            PostEntityResponse listingResponse = parser.parseListingResponse(response);

            if (listingResponse.getSuccess().equalsIgnoreCase("true")) {
                dataObjectsArray.clear();
                ArrayList<PostEntity> newList = listingResponse.getData();
                dataObjectsArray.addAll(newList);
                Log.d("EntityData : ", "Count of entities while setting the adaptar -- " + dataObjectsArray.size());
                postadapter.notifyDataSetChanged();
            }
        }

        else if (action.equalsIgnoreCase("API_ACTION_SHARE") && response != null) {

            Log.d("Share Response", "Response from Sharing on Social Media --> " + response);

        }
    }

    public void clickEvent(View v) {
        if (v.getId() == R.id.iv_use1r) {
            Intent in = new Intent(PostActivity.this, ProfileActivity.class);
            startActivity(in);
        } else if (v.getId() == R.id.iv_que) {
            Intent in = new Intent(PostActivity.this, QueueActivity.class);
            startActivity(in);
        }
    }
}

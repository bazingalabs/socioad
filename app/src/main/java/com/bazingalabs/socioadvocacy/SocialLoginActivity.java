package com.bazingalabs.socioadvocacy;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.StrictMode;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.Profile;
import com.facebook.ProfileTracker;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class SocialLoginActivity extends Activity {

    Button btn_linkedIn, btn_twiteer;
    Button btn_next, btn_skip;
    CallbackManager callbackManager;
    AccessTokenTracker accessTokenTracker;
    TextView tvProfile;
    LoginButton fbLoginButton;
    SharedPreferences pref;
    SharedPreferences.Editor editor;
    ProgressDialog pd;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Facebook Details
        FacebookSdk.sdkInitialize(getApplicationContext());
        getFbKeyHash("com.bazingalabs.socioadvocacy");
        callbackManager = CallbackManager.Factory.create();


        //Layout
        setContentView(R.layout.activity_social_login);
        fbLoginButton = (LoginButton) findViewById(R.id.btn_fb_login);
        fbLoginButton.setPublishPermissions(Arrays.asList("publish_actions"));
        btn_next = (Button) findViewById(R.id.btn_next_social);
        btn_skip = (Button) findViewById(R.id.btn_skip_social);
        tvProfile = (TextView) findViewById(R.id.tvProfName);
        btn_twiteer=(Button)findViewById(R.id.btn_Twiteer_SignIn_social);
        btn_linkedIn=(Button)findViewById(R.id.btn_LinkedIn_SignIn_social);


        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        editor = pref.edit();

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
            }
        };
        accessTokenTracker.startTracking();



        //Facebook callback registration
        fbLoginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.v("Facebook", "Successfully logged in to Facebook!");
                AccessToken accessToken = loginResult.getAccessToken();
                editor.putString("facebookAccessToken", loginResult.getAccessToken().getToken());
                editor.putString("facebookID", loginResult.getAccessToken().getUserId());
                editor.putString("isFacebook", "yes");
                editor.commit();

                (new updateTokensToServer()).execute();
            }
            @Override
            public void onCancel() {
                Toast.makeText(SocialLoginActivity.this, "Login Cancel", Toast.LENGTH_LONG).show();
            }
            @Override
            public void onError(FacebookException error) {
                Toast.makeText(SocialLoginActivity.this, "Oops, something went wrong", Toast.LENGTH_LONG).show();
            }
        });


        btn_twiteer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loginWithTwitter();
            }
        });

        btn_linkedIn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                loginWithLinkedin();

            }
        });

        btn_next.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            if (checkIfAnyLoggedIn()) {
                                                Intent in = new Intent(SocialLoginActivity.this, PostActivity.class);
                                                startActivity(in);
                                            } else {
                                                Toast.makeText(SocialLoginActivity.this, "Please login with any one social media!", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    }
            );

        btn_skip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (checkIfAnyLoggedIn()) {
                    Intent in = new Intent(SocialLoginActivity.this, PostActivity.class);
                    startActivity(in);
                } else {
                    Toast.makeText(SocialLoginActivity.this, "Please login with any one social media!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }




    private boolean checkIfAnyLoggedIn() {
        //Check for one of the social media
        String isFacebook = pref.getString("isFacebook","no");
        String isTwitter = pref.getString("isTwitter","no");
        String isLinkedin = pref.getString("isLinkedin","no");
        if(isFacebook.equalsIgnoreCase("yes") || isTwitter.equalsIgnoreCase("yes") || isLinkedin.equalsIgnoreCase("yes") )
        {
            return true;
        }
        return false;
    }

    private void loginWithLinkedin() {
        //Login Via Linkedin
        Intent in = new Intent(SocialLoginActivity.this, PostActivity.class);
        startActivity(in);
    }



    private void loginWithTwitter() {
        //Login Via Linkedin
        Intent in = new Intent(SocialLoginActivity.this, PostActivity.class);
        startActivity(in);
    }



    //Facebook Hash Function
    public void getFbKeyHash(String packageName) {

        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    packageName,
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("YourKeyHash :", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                System.out.println("YourKeyHash: " + Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }
    @Override
    public void onResume() {
        super.onResume();
        AppEventsLogger.activateApp(SocialLoginActivity.this);
    }

    @Override
    public void onPause() {
        super.onPause();
        AppEventsLogger.deactivateApp(SocialLoginActivity.this);
    }
    @Override
    public void onStop() {
        super.onStop();
        accessTokenTracker.stopTracking();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        accessTokenTracker.stopTracking();
    }


    public class updateTokensToServer extends AsyncTask<Void, Void, Void> {
        @Override
        protected void onPreExecute() {
            pd = new ProgressDialog(SocialLoginActivity.this);
            pd.setMessage("Please wait while we connect you to social media!");
            pd.setCancelable(false);
            pd.setCanceledOnTouchOutside(false);
            pd.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {

                //Update All Social Media Tokens on Server
                String isFacebook = pref.getString("isFacebook","no");
                String isTwitter = pref.getString("isTwitter","no");
                String isLinkedin = pref.getString("isLinkedin","no");

                if (isFacebook.equalsIgnoreCase("yes"))
                {
                    DefaultHttpClient httpclient = new DefaultHttpClient();
                    HttpPost httppost = new HttpPost("http://api.socioadvocacy.com/user/sauth?access_token=d481c88e3fa43711314fb0e262ca6a088a7a9415");
                    HttpEntity httpEntity = null;
                    String response = null;

                    List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                    String uid = pref.getString("uid","Error");
                    String accessToken = pref.getString("facebookAccessToken","Error");
                    nameValuePairs.add(new BasicNameValuePair("uid", uid));
                    nameValuePairs.add(new BasicNameValuePair("snetwork", "fb"));
                    nameValuePairs.add(new BasicNameValuePair("token", accessToken));
                    httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                    // Execute HTTP Post Request
                    HttpResponse httpResponse = httpclient.execute(httppost);
                    httpEntity = httpResponse.getEntity();
                    response = EntityUtils.toString(httpEntity);

                    Log.v("Facebook", "Facebook Access Key Update Response --> " + response);
                }


                return null;

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

        }

        @Override
        protected void onPostExecute(Void result) {
            pd.dismiss();
        }
    }
}

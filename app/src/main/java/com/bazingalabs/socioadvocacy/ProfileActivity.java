package com.bazingalabs.socioadvocacy;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.bazingalabs.socioadvocacy.Parser.LeaderResponseParser;
import com.bazingalabs.socioadvocacy.adapter.LeaderAdapter;
import com.bazingalabs.socioadvocacy.bo.LeaderResponse;
import com.bazingalabs.socioadvocacy.network.NetworkTask;


public class ProfileActivity extends Activity implements NetworkTask.RequestCompleteListener {

    ListView lv_lead;
    Button btn_getStarted;
    String cid, uid, fb_access_token;
    String access_token = "d481c88e3fa43711314fb0e262ca6a088a7a9415";
    SharedPreferences pref;
    LeaderAdapter leaderadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        btn_getStarted = (Button) findViewById(R.id.btn_getStarted);

        TextView tvprof = (TextView) findViewById(R.id.tvProfName);

        lv_lead=(ListView)findViewById(R.id.lv_leaderboard);

        pref = getApplicationContext().getSharedPreferences("MyPref", 0);
        cid = pref.getString("cid", null);
        uid = pref.getString("uid", null);

        tvprof.setText("test");
        getList();
        btn_getStarted.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(ProfileActivity.this, PostActivity.class);
                startActivity(in);

            }
        });

    }

    private void getList() {
        (new NetworkTask(ProfileActivity.this, this, "API_ACTION_GETLISTING", "Please wait", 102, "http://api.socioadvocacy.com/company/leaderboard/?access_token="+access_token+"&cid="+cid)).execute(new Void[0]);


    }


    @Override
    public void onRequestComplete(String response, String action) {
        if (action.equalsIgnoreCase("API_ACTION_GETLISTING") && response != null) {
            Log.d("**", "" + response);
            LeaderResponse listingResponse = LeaderResponseParser.parseListingResponse(response);
            if (listingResponse.getSuccess().equalsIgnoreCase("true")) {
                //setAdapter(entities);
                Log.d("**True", "");
                Log.d("geting data",""+listingResponse.getData());
                leaderadapter = new LeaderAdapter(ProfileActivity.this, listingResponse.getData());
               lv_lead .setAdapter(leaderadapter);
                leaderadapter.notifyDataSetChanged();
            }
        }
    }

}

package com.bazingalabs.socioadvocacy;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.bazingalabs.socioadvocacy.Parser.QueueListingResponseParser;
import com.bazingalabs.socioadvocacy.adapter.QueueEntityAdapter;
import com.bazingalabs.socioadvocacy.bo.QueueEntity;
import com.bazingalabs.socioadvocacy.bo.QueueListingResponse;
import com.bazingalabs.socioadvocacy.network.NetworkTask;

import java.util.ArrayList;


public class QueueActivity extends Activity implements NetworkTask.RequestCompleteListener{

    ListView lv_queue;
    private ArrayList<QueueEntity> entities;
    String cid="8e6c1aa7", uid="9632855f";
    String access_token = "d481c88e3fa43711314fb0e262ca6a088a7a9415";
    SharedPreferences pref;
    QueueEntityAdapter entityadapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_queue);

        lv_queue = (ListView)findViewById(R.id.listView);
//        cid = pref.getString("cid", null);


        getList();

    }
    private void getList() {
        (new NetworkTask(QueueActivity.this, this, "API_ACTION_GETLISTING", "Please wait", 102, "http://api.socioadvocacy.com/feed/schedule/list/?cid="+cid+"&uid="+uid+"&access_token="+access_token)).execute(new Void[0]);
        // For testing http://api.socioadvocacy.com/company/feed/2015-01-01/2015-03-10?cid=8e6c1aa7&uid=9632855faccess_token=d481c88e3fa43711314fb0e262ca6a088a7a9415

    }

    @Override
    public void onRequestComplete(String response, String action) {
        if (action.equalsIgnoreCase("API_ACTION_GETLISTING") && response != null) {
            Log.d("**", "" + response);
            QueueListingResponse listingResponse = QueueListingResponseParser.parseListingResponse(response);
            if (listingResponse.getSuccess().equalsIgnoreCase("true")) {
                Log.d("**True", "");
                Log.d("geting data",""+listingResponse.getData());
                entityadapter = new QueueEntityAdapter(QueueActivity.this, listingResponse.getData());
                lv_queue.setAdapter(entityadapter);
                entityadapter.notifyDataSetChanged();
            }



        }
    }
}

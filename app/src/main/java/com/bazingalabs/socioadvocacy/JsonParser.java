package com.bazingalabs.socioadvocacy;

import android.util.Log;
import android.widget.ListView;

import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.List;

/**
 * Created by apple on 01/07/15.
 */
public class JsonParser {
    static InputStream is = null;
    static JSONObject jObj = null;
    static String json = "";
    String URL;
    String DEFAULT_URL;
    String paramString;
    JSONObject c;
    BufferedReader buffer;
    HashMap cars;
    ListView filteredCarsListView;
    JSONObject jsonObj;
    JSONArray jsonarray;
    StringBuilder sb;
    DefaultHttpClient defaultHttpClient;

    public JsonParser() {
        DEFAULT_URL = "http://api.socioadvocacy.com/";
        URL = "http://api.socioadvocacy.com/";
        jsonarray = null;
        cars = new HashMap();
    }

    public JSONObject makeHttpRequest(String s, String s1, List list) {
        s1 = (new StringBuilder(String.valueOf(DEFAULT_URL))).append(s1).toString();
        if (s == "GET") {
            try {



                defaultHttpClient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(s1);
                httppost.setEntity(new UrlEncodedFormEntity(list));
                Log.e("***********strr", s1);
                is = defaultHttpClient.execute(httppost).getEntity().getContent();
                try {
                    // Misplaced declaration of an exception variable
                    buffer = new BufferedReader(new InputStreamReader(is, "iso-8859-1"),8);
                    sb = new StringBuilder();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                paramString = buffer.readLine();
                if (paramString != null) {
                    try {
                        sb.append((new StringBuilder(String.valueOf(list))).append("\n").toString());
;                        Log.e("JSon responce", (new StringBuilder()).append(json).toString());
                        jsonObj = new JSONObject(json);
                        is.close();
                    }
                    // Misplaced declaration of an exception variable
                    catch (Exception e) {
                        Log.e("Buffer Error", (new StringBuilder("Error converting result ")).append(s.toString()).toString());
                    }
                    return jsonObj;
                }
            }
            // Misplaced declaration of an exception variable
            catch (Exception e) {
                e.printStackTrace();
            }
            // Misplaced declaration of an exception variable


        } else {
            if (s == "POST") {

                try {
                    defaultHttpClient = new DefaultHttpClient();
                    paramString = URLEncodedUtils.format(list, "UTF-8");
                    s1 = (new StringBuilder(String.valueOf(s1))).append("?").append(paramString).toString();
                    Log.e("str******", s1);
                    is = defaultHttpClient.execute(new HttpGet(s1)).getEntity().getContent();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return jObj;
    }

    public JSONObject makeHttpRequestForCid(String s, String s1, List<NameValuePair> list) {
        s1 = (new StringBuilder(String.valueOf(URL))).append(s1).toString();
        if (s == "POST") {
            try {
                defaultHttpClient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost(s1);
                httppost.setEntity(new UrlEncodedFormEntity(list));
                Log.e("strrrrrrr", s1);
                is = defaultHttpClient.execute(httppost).getEntity().getContent();
            }
            // Misplaced declaration of an exception variable
            catch (Exception e) {
                e.printStackTrace();
            }
            try {
                // Misplaced declaration of an exception variable
                buffer = new BufferedReader(new InputStreamReader(is, "UTF-8"));
                sb = new StringBuilder();
            } catch (Exception e) {

                e.printStackTrace();
            }
        } else {
            try {
                defaultHttpClient = new DefaultHttpClient();
                paramString = URLEncodedUtils.format(list, "utf-8");
                s1 = (new StringBuilder(String.valueOf(s1))).append("?").append(list).toString();
                Log.e("strrrrrrr", s1);
                is = defaultHttpClient.execute(new HttpGet(s1)).getEntity().getContent();

                sb.append((new StringBuilder(String.valueOf(list))).append("\n").toString());
                paramString = buffer.readLine();
                if (paramString == null) {
                    try {
                        json = sb.toString();
                        Log.e("JSon responce", (new StringBuilder()).append(json).toString());
                        jObj = new JSONObject(json);
                        is.close();
                    }
                    // Misplaced declaration of an exception variable
                    catch (Exception e) {
                        Log.e("Buffer Error", (new StringBuilder("Error converting result ")).append(e.toString()).toString());
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } return jObj;
    }
}
